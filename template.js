let templates = {
    "explanation":
        `___guideText___
        <div id="alert" class="alert alert-info" style="display:none;" role="alert" align="center">
            <strong>___nextAlertText___</strong>
        </div>`,
    "cross":
        `<br><br><br><br><br><br><br><br><br>
        <div align="center">
            <h1>+</h1>
        </div>
        `,
    "question":
        `<div align="center">
            <h1>___type___ ___seq___</h1>
            <div id="remainingOuter" style="display:none;"><h4>남은 시간: <span id="remaining"></span>초</h4></div>
        </div>
        <div class="progress">
            <div id="progressbar" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 100%;"><span class="sr-only">remaining</span></div>
        </div>
        <div class="jumbotron">
            <h3 align="center">___questionText___</h3>
        </div>
        <div id="alert" class="alert alert-info" style="display:none;" role="alert" align="center">
            <strong>___nextAlertText___</strong>
        </div>
        <div id="timeLimitAlert" class="alert alert-danger" style="display:none;" role="alert" align="center">
            <strong>___timeLimitAlertText___</strong>
        </div>`,
    "answer":
        `<div align="center">
            <h1>___type___ ___seq___</h1>
            <div id="remainingOuter" style="display:none;"><h4>남은 시간: <span id="remaining"></span>초</h4></div>
        </div>
        <div class="progress">
            <div id="progressbar" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="display:none;width: 100%;"><span class="sr-only">remaining</span></div>
        </div>
        <div id="questionText" class="jumbotron" style="display:none;">
            <h3 align="center">___questionText___</h3>
        </div>
        <div id="optionsYN" class="row" style="display:none;">
            <div class="col-xs-6" align="center">
                <button id="buttonY" type="button" class="btn btn-lg btn-primary">네(Y)</button>
            </div>
            <div class="col-xs-6" align="center">
                <button id="buttonN" type="button" class="btn btn-lg btn-danger">아니오(N)</button>
            </div>
        </div>
        <div id="options7" class="row" style="display:none;">
            <div class="col-xs-12" align="center">
                <h3>←
                <button id="button1" type="button" class="btn btn-lg btn-danger">1</button> -
                <button id="button2" type="button" class="btn btn-lg btn-warning">2</button> -
                <button id="button3" type="button" class="btn btn-lg btn-warning">3</button> -
                <button id="button4" type="button" class="btn btn-lg btn-info">4</button> -
                <button id="button5" type="button" class="btn btn-lg btn-success">5</button> -
                <button id="button6" type="button" class="btn btn-lg btn-success">6</button> -
                <button id="button7" type="button" class="btn btn-lg btn-primary">7</button>
                →</h3>
            </div>
            <div class="row">
                <div class="col-xs-6" align="center"><h4>(매우 어색함)</h4></div>
                <div class="col-xs-6" align="center"><h4>(매우 자연스러움)</h4></div>
            </div>
        </div>
        <br>
        <div id="alert" class="alert alert-info" style="display:none;" role="alert" align="center">
            <strong>___nextAlertText___</strong>
        </div>
        <div id="timeLimitAlert" class="alert alert-danger" style="display:none;" role="alert" align="center">
            <strong>___timeLimitAlertText___</strong>
        </div>
        <div id="statsAlert" class="alert alert-warning" style="display:none;" role="alert" align="center"></div>
        <!--<div id="statsAlert" style="display:none;"><h1><span class="label label-primary"></span></h1></div>-->`
}