let actions = {
    "explanation": function (params, screenIndex) {
        // let screen = screenList[screenIndex]
        contentTarget.innerHTML = getTemplated (templates.explanation, params)
        if (params.save) save()
        // console.log("screenList: ", screenList)
        if (screenIndex === screenList.length - 1) return // 마지막 스크린이면 텍스트 표시만 하고 끝
        let timer
        let startTime = new Date()
        let inProcessingKey = false
        let keyDownHandler = function (event) {
            if (inProcessingKey) return
            inProcessingKey = true
            // console.log('pressed: ', event.keyCode)
            if (event.keyCode !== 32 || (params.minimumDuration && (new Date() - startTime < params.minimumDuration))) {
                inProcessingKey = false
                return
            }
            removeKeydownHandler(keyDownHandler)
            if (timer) clearTimeout(timer)
            doScreenAction(screenIndex + 1)
        }
        addKeydownHandler(keyDownHandler)
        if (params.duration) {
            timer = setTimeout(function () {
                document.getElementById("alert").style.display = "block"
            }, params.duration)
        }
    },
    "cross": function (params, screenIndex) {
        contentTarget.innerHTML = getTemplated (templates.cross, params)
        if (screenIndex === screenList.length - 1) return
        let timer = setTimeout(function () {
            doScreenAction(screenIndex + 1)
        }, params.duration)
    },
    "question": function (params, screenIndex) {
        contentTarget.innerHTML = getTemplated (templates.question, params)
        let screen = screenList[screenIndex]
        if (screenIndex === screenList.length - 1) return
        let alertTimer, limitTimer
        let intervalTimerForProgressBar, milliSecPassedForProgressBar = 0
        let intervalTimerForRemaining, milliSecPassedForRemaining = 0
        let startTime = new Date()
        let inProcessingKey = false

        let keyDownHandler = function (event) {
            if (inProcessingKey) return
            inProcessingKey = true
            // console.log('pressed')
            let elapsedTime = new Date() - startTime
            if (event.keyCode !== 32 || (params.minimumDuration && (elapsedTime < params.minimumDuration))) {
                inProcessingKey = false
                return
            }
            removeKeydownHandler(keyDownHandler)
            if (alertTimer) clearTimeout(alertTimer)
            if (limitTimer) clearTimeout(limitTimer)
            if (intervalTimerForProgressBar) clearInterval(intervalTimerForProgressBar)
            if (intervalTimerForRemaining) clearInterval(intervalTimerForRemaining)
            screen.result = {
                elapsedTime: elapsedTime
            }
            doScreenAction(screenIndex + 1)
        }
        addKeydownHandler(keyDownHandler)

        if (params.duration) {
            alertTimer = setTimeout(function () {
                document.getElementById("alert").style.display = "block"
            }, params.duration)
        }

        if (params.timeLimit) {
            limitTimer = setTimeout(function () {
                inProcessingKey = true
                // console.log('TIME OVER')
                removeKeydownHandler(keyDownHandler)
                screen.result = {
                    elapsedTime: params.timeLimit,
                    isTimedOut: true
                }
                if (isNextScreenIsAnswerToCurrentScreen(screenIndex)) {
                    // console.log('다음 screen(answer화면) 의 result를 오답으로 처리')
                    let nextScreen = screenList[screenIndex + 1]
                    nextScreen.result = {
                        elapsedTime: 0,
                        answer: undefined,
                        isRight: nextScreen.variables.rightOption ? false : undefined,
                        isAlreadyTimedOutByQuestionScreen: true
                    }
                }
                let nextScreenIdToGo = isNextScreenIsAnswerToCurrentScreen(screenIndex) ? screenIndex + 2 : screenIndex + 1
                if (params.timeLimitAlertText && params.timeLimitAlertTextDuration) {
                    document.getElementById("timeLimitAlert").style.display = "block"
                    let limitAlertTimer = setTimeout(function () {
                        // console.log('go to next question')
                        doScreenAction(nextScreenIdToGo)
                    }, params.timeLimitAlertTextDuration)
                } else {
                    // console.log('go to next question')
                    doScreenAction(nextScreenIdToGo)
                }
            }, params.timeLimit)

            document.getElementById("progressbar").style.display = "block"
            intervalTimerForProgressBar = setInterval(function () { // 처음에는 0.5초 경과 후 한 칸 줄임 (줄어드는 데 시간이 0.5초 정도 걸림)
                milliSecPassedForProgressBar += 1000
                setProgressBar((1 - (milliSecPassedForProgressBar / params.timeLimit)) * 100)
                clearInterval(intervalTimerForProgressBar)
                intervalTimerForProgressBar = setInterval(function () { // 이후에는 1초마다 한 칸 줄임
                    if (milliSecPassedForProgressBar === params.timeLimit) {
                        clearInterval(intervalTimerForProgressBar)
                        return
                    }
                    milliSecPassedForProgressBar += 1000
                    setProgressBar((1 - (milliSecPassedForProgressBar / params.timeLimit)) * 100)
                }, 1000)
            }, 500)
            
            document.getElementById("remainingOuter").style.display = "block"
            setRemaining(params.timeLimit / 1000)
            intervalTimerForRemaining = setInterval(function () {
                milliSecPassedForRemaining += 1000
                setRemaining((params.timeLimit - milliSecPassedForRemaining) / 1000)
                if (milliSecPassedForRemaining === params.timeLimit) {
                    clearInterval(intervalTimerForRemaining)
                    return
                }
            }, 1000)
        }
    },
    "answer": function (params, screenIndex) {
        contentTarget.innerHTML = getTemplated (templates.answer, params)
        if (params.questionText) document.getElementById("questionText").style.display = "block"
        let screen = screenList[screenIndex]
        if (screenIndex === screenList.length - 1) return
        let alertTimer, limitTimer
        let intervalTimerForProgressBar, milliSecPassedForProgressBar = 0
        let intervalTimerForRemaining, milliSecPassedForRemaining = 0
        let startTime = new Date()
        let inProcessingKey = false

        let keyDownHandler = function (event) {
            if (inProcessingKey) return
            inProcessingKey = true
            let selected = getOptionValueByPressedKey(params.optionFormat, event.keyCode)
            // console.log('selected: ', selected)
            if (!selected) {
                inProcessingKey = false
                return // 선택지 중 하나에 해당하지 않는 키 눌렀을 경우 무시
            }
            let elapsedTime = new Date() - startTime
            if (params.minimumDuration && (elapsedTime < params.minimumDuration)) {
                inProcessingKey = false
                return
            }
            removeKeydownHandler(keyDownHandler)
            if (alertTimer) clearTimeout(alertTimer)
            if (limitTimer) clearTimeout(limitTimer)
            if (intervalTimerForProgressBar) clearInterval(intervalTimerForProgressBar)
            if (intervalTimerForRemaining) clearInterval(intervalTimerForRemaining)
            screen.result = {
                elapsedTime: elapsedTime,
                answer: selected,
                isRight: params.rightOption ? selected === params.rightOption : undefined,
                inputDevice: 'keyboard'
            }
            if (params.save) save()
            let statsText = getStats(screenIndex)
            if (statsText) {
                document.getElementById("statsAlert").innerHTML = `<strong>${statsText}</strong>`
                document.getElementById("statsAlert").style.display = "block"
                let statsTimer = setTimeout(function () {
                    doScreenAction(screenIndex + 1)
                }, env.statsDuration)
            } else {
                doScreenAction(screenIndex + 1)
            }
        }

        let textKeyDownHandler = function (event) {
            if (inProcessingKey) return
            inProcessingKey = true
            if (event.keyCode !== 32) {
                inProcessingKey = false
                return
            }
            if (params.minimumDuration && (new Date() - startTime < params.minimumDuration)) {
                inProcessingKey = false
                return
            }
            removeKeydownHandler(textKeyDownHandler)
            addKeydownHandler(keyDownHandler)
            document.getElementById("options" + params.optionFormat).style.display = "block"
            document.getElementById("alert").style.display = "none"
            inProcessingKey = false
        }

        let buttonClickHandler = function (selected) {
            if (!params.mouseEnabled) return
            if (inProcessingKey) return
            inProcessingKey = true
            let elapsedTime = new Date() - startTime
            if (params.minimumDuration && (elapsedTime < params.minimumDuration)) {
                inProcessingKey = false
                return
            }
            removeKeydownHandler(keyDownHandler)
            if (alertTimer) clearTimeout(alertTimer)
            if (limitTimer) clearTimeout(limitTimer)
            if (intervalTimerForProgressBar) clearInterval(intervalTimerForProgressBar)
            if (intervalTimerForRemaining) clearInterval(intervalTimerForRemaining)
            screen.result = {
                elapsedTime: elapsedTime,
                answer: selected,
                isRight: params.rightOption ? selected === params.rightOption : undefined,
                inputDevice: 'mouse'
            }
            if (params.save) save()
            let statsText = getStats(screenIndex)
            if (statsText) {
                document.getElementById("statsAlert").innerHTML = `<strong>${statsText}</strong>`
                document.getElementById("statsAlert").style.display = "block"
                let statsTimer = setTimeout(function () {
                    doScreenAction(screenIndex + 1)
                }, env.statsDuration)
            } else {
                doScreenAction(screenIndex + 1)
            }
        }

        if (params.keyRequiredBeforeDisplayOptions) {
            addKeydownHandler(textKeyDownHandler)
        } else {
            document.getElementById("options" + params.optionFormat).style.display = "block"
            addKeydownHandler(keyDownHandler)
        }

        if (params.mouseEnabled) {
            if (params.optionFormat === '7') {
                if (document.getElementById("button1")) document.getElementById("button1").addEventListener('click', function(e) { buttonClickHandler('1')})
                if (document.getElementById("button2")) document.getElementById("button2").addEventListener('click', function(e) { buttonClickHandler('2')})
                if (document.getElementById("button3")) document.getElementById("button3").addEventListener('click', function(e) { buttonClickHandler('3')})
                if (document.getElementById("button4")) document.getElementById("button4").addEventListener('click', function(e) { buttonClickHandler('4')})
                if (document.getElementById("button5")) document.getElementById("button5").addEventListener('click', function(e) { buttonClickHandler('5')})
                if (document.getElementById("button6")) document.getElementById("button6").addEventListener('click', function(e) { buttonClickHandler('6')})
                if (document.getElementById("button7")) document.getElementById("button7").addEventListener('click', function(e) { buttonClickHandler('7')})
            } else if (params.optionFormat === 'YN') {
                if (document.getElementById("buttonY")) document.getElementById("buttonY").addEventListener('click', function(e) { buttonClickHandler('Y')})
                if (document.getElementById("buttonN")) document.getElementById("buttonN").addEventListener('click', function(e) { buttonClickHandler('N')})
            }
        }

        if (params.duration) {
            alertTimer = setTimeout(function () {
                document.getElementById("alert").style.display = "block"
            }, params.duration)
        }

        if (params.timeLimit) {
            limitTimer = setTimeout(function () {
                inProcessingKey = true
                // console.log('TIME OVER')
                removeKeydownHandler(textKeyDownHandler)
                removeKeydownHandler(keyDownHandler)
                screen.result = {
                    elapsedTime: params.timeLimit,
                    answer: undefined,
                    isRight: params.rightOption ? false : undefined,
                    isTimedOut: true
                }
                if (params.timeLimitAlertText && params.timeLimitAlertTextDuration) {
                    document.getElementById("timeLimitAlert").style.display = "block"

                    let statsText = getStats(screenIndex)
                    if (statsText) {
                        document.getElementById("statsAlert").innerHTML = `<strong>${statsText}</strong>`
                        document.getElementById("statsAlert").style.display = "block"
                    }

                    let limitAlertTimer = setTimeout(function () {
                        // console.log('go to next question')
                        if (params.save) save()
                        doScreenAction(screenIndex + 1)
                    }, params.timeLimitAlertTextDuration)
                } else {
                    if (params.save) save()
                    let statsText = getStats(screenIndex)
                    if (statsText) {
                        document.getElementById("statsAlert").innerHTML = `<strong>${statsText}</strong>`
                        document.getElementById("statsAlert").style.display = "block"
                        let statsTimer = setTimeout(function () {
                            doScreenAction(screenIndex + 1)
                        }, env.statsDuration)
                    } else {
                        doScreenAction(screenIndex + 1)
                    }
                }
            }, params.timeLimit)

            document.getElementById("progressbar").style.display = "block"
            intervalTimerForProgressBar = setInterval(function () { // 처음에는 0.5초 경과 후 한 칸 줄임 (줄어드는 데 시간이 0.5초 정도 걸림)
                milliSecPassedForProgressBar += 1000
                setProgressBar((1 - (milliSecPassedForProgressBar / params.timeLimit)) * 100)
                clearInterval(intervalTimerForProgressBar)
                intervalTimerForProgressBar = setInterval(function () { // 이후에는 1초마다 한 칸 줄임
                    if (milliSecPassedForProgressBar === params.timeLimit) {
                        clearInterval(intervalTimerForProgressBar)
                        return
                    }
                    milliSecPassedForProgressBar += 1000
                    setProgressBar((1 - (milliSecPassedForProgressBar / params.timeLimit)) * 100)
                }, 1000)
            }, 500)
            
            document.getElementById("remainingOuter").style.display = "block"
            setRemaining(params.timeLimit / 1000)
            intervalTimerForRemaining = setInterval(function () {
                milliSecPassedForRemaining += 1000
                setRemaining((params.timeLimit - milliSecPassedForRemaining) / 1000)
                if (milliSecPassedForRemaining === params.timeLimit) {
                    clearInterval(intervalTimerForRemaining)
                    return
                }
            }, 1000)
        }
    }
}

let getTemplated = function (template, params) {
    let templated = template
    let matches = template.match(/___[A-Za-z0-9]+___/g)
    if (matches) {
        for (let match of matches) {
            templated = templated.replace(match, params[match.replace(/_/g, '')])
        }
    }
    return templated
}

let addKeydownHandler = function (handler) {
    if (document.addEventListener) {
        document.addEventListener('keydown', handler, false)
    }
    else if (document.attachEvent) {
        document.attachEvent('onkeydown', handler)
    }
}

let removeKeydownHandler = function (handler) {
    if (document.removeEventListener) {
        document.removeEventListener('keydown', handler, false)
    }
    else if (document.detachEvent) {
        document.detachEvent('onkeydown', handler)
    }
}

let doScreenAction = function (screenIndex) {
    let screen = screenList[screenIndex]
    let defaultsAddedVariables = {}
    Object.assign(defaultsAddedVariables, defaults[screen.phase] || {}, screen.variables || {})
    actions[screen.phase](defaultsAddedVariables, screenIndex)
}

// 다음번 스크린이 이번 스크린(question)에 대한 answer인지 판별
// 예1: screen 7이 [part 1 실제문제 3번에 대한 question 화면] 이고 screen 8이 [part 1 실제문제 3번에 대한 answer 화면] 이면 isNextScreenIsAnswerToCurrentScreen(7) = true
// 예2: screen 7이 [part 1 실제문제 3번에 대한 question 화면] 이고 screen 8이 [part 1 실제문제 3번에 대한 answer 화면] 이 아니면 isNextScreenIsAnswerToCurrentScreen(7) = false
let isNextScreenIsAnswerToCurrentScreen = function (currentScreenIndex) {
    let screen = screenList[currentScreenIndex]
    if (screen.phase !== 'question') return false // 애초에 이번 스크린이 문제를 표시하는 화면이 아닌 경우
    if (currentScreenIndex >= screenList.length - 1) return false // 이번 스크린이 마지막인 경우 
    let nextScreen = screenList[currentScreenIndex + 1]
    if (nextScreen.phase === 'answer' &&
        screen.variables.part === nextScreen.variables.part &&
        screen.variables.type === nextScreen.variables.type &&
        screen.variables.no === nextScreen.variables.no) return true
    return false
}

let getOptionValueByPressedKey = function (optionFormat, keyCode) {
    keyCode = keyCode.toString()
    if (keyCodes[optionFormat]) return keyCodes[optionFormat][keyCode]
    return undefined
}

let save = function () {
    // console.log('saving: ', fileName)
    let resultList = []
    for (let screen of screenList) {
        if (screen.result) {
            let result = {}
            Object.assign(result, screen.result)
            result.phase = screen.phase
            result.part = screen.variables.part
            result.type = screen.variables.type
            result.no = screen.variables.no
            result.seq = screen.variables.seq
            resultList.push(result)
        }
    }
    let resultListText = JSON.stringify(resultList, null, 4)
    let blob = new Blob([resultListText], {type: "text/plain;charset=utf-8"})
    saveAs(blob, fileName)
}

let pad2 = function (n) {
    return n < 10 ? '0' + n : '' + n
}

let randomizeOrder = function (srcList) {
    let listOfGroup = [] // [문제] 또는 [문제, 문제에 대한 답] 또는 [문제, 문제외의 것들 N개, 문제에 대한 답] 꼴의 배열을 요소로 가지는 배열

    // 1. srcList 데이터를 이용해서 listOfGroup 을 채운다.
    let openedQuestion
    for (let screen of srcList) {
        if (screen.phase === 'question') {
            listOfGroup.push([screen])
            openedQuestion = {
                part: screen.variables.part,
                type: screen.variables.type,
                no: screen.variables.no
            }
        } else if (screen.phase === 'answer') {
            if (openedQuestion &&
                openedQuestion.part === screen.variables.part &&
                openedQuestion.type === screen.variables.type &&
                openedQuestion.no === screen.variables.no
            ) {
                listOfGroup[listOfGroup.length - 1].push(screen)
            } else {
                listOfGroup.push([screen])
            }
            openedQuestion = undefined
        } else {
            if (openedQuestion) {
                listOfGroup[listOfGroup.length - 1].push(screen)
            } else {
                listOfGroup.push([screen])
            }
        }
    }
    // console.log('listOfGroup: ', JSON.stringify(listOfGroup, null, 4))

    // 2. < listOfGroup의 원소(=배열)들 중 문제 또는 답을 포함하는 것(=배열)들> 들로 listOfGroupToBeRandomized (랜덤화 적용 대상)를 만든다.
    //    이때 listOfGroupToBeRandomized를 만드는 데 사용된 listOfGroup의 원소 자리의 값은 []로 변경한다.
    let listOfGroupToBeRandomized = []
    for (let i = 0; i < listOfGroup.length; i++) {
        let groupHasQuestionOrAnswer = false
        let targetGroup = []
        for (let screen of listOfGroup[i]) {
            if (screen.phase === 'question' || screen.phase === 'answer') groupHasQuestionOrAnswer = true
            targetGroup.push(screen)
        }
        if (groupHasQuestionOrAnswer) {
            listOfGroupToBeRandomized.push(targetGroup)
            listOfGroup[i] = []
        }
    }
    // console.log('listOfGroupToBeRandomized: ', JSON.stringify(listOfGroupToBeRandomized, null, 4))
    // console.log('listOfGroup: ', JSON.stringify(listOfGroup, null, 4))

    // 3. listOfGroupToBeRandomized의 원소들의 순서를 랜덤화한다. 전체건 대상 한번 수행이 아니라 part/type별로 나누어서 수행한다.
    let randomizedListOfGroupToBeRandomized = []
    let partToBeShuffled = []
    let lastPart = ''
    let lastType = ''
    for (let i = 0; i < listOfGroupToBeRandomized.length; i++) {
        let groupFirstScreen = listOfGroupToBeRandomized[i][0]
        if (groupFirstScreen.variables.part === lastPart && groupFirstScreen.variables.type === lastType) {
            partToBeShuffled.push(listOfGroupToBeRandomized[i])
        } else {
            if (partToBeShuffled.length > 0) {
                shuffle(partToBeShuffled)
                let groupSeq = 0
                for (let group of partToBeShuffled) {
                    groupSeq++
                    for (let screen of group) {
                        if (screen.phase === 'question' || screen.phase === 'answer') {
                            screen.variables.seq = groupSeq
                        }
                    }
                }
                randomizedListOfGroupToBeRandomized = randomizedListOfGroupToBeRandomized.concat(partToBeShuffled)
            }
            partToBeShuffled = [listOfGroupToBeRandomized[i]]
            lastPart = groupFirstScreen.variables.part
            lastType = groupFirstScreen.variables.type
        }
    }
    if (partToBeShuffled.length > 0) { // for loop 완료후 남아있는 건들 처리
        shuffle(partToBeShuffled)
        let groupSeq = 0
        for (let group of partToBeShuffled) {
            groupSeq++
            for (let screen of group) {
                if (screen.phase === 'question' || screen.phase === 'answer') {
                    screen.variables.seq = groupSeq
                }
            }
        }
        randomizedListOfGroupToBeRandomized = randomizedListOfGroupToBeRandomized.concat(partToBeShuffled)
    }
    // console.log('randomizedListOfGroupToBeRandomized: ', JSON.stringify(randomizedListOfGroupToBeRandomized, null, 4))

    // 4. listOfGroupToBeRandomized의 원소가 빈 배열인 곳에 randomizedListOfGroupToBeRandomized의 원소를 대신 하나씩 넣는다.
    let ramdomizedListIndex = 0
    for (let i = 0; i < listOfGroup.length; i++) {
        if (listOfGroup[i].length === 0) {
            for (let el of randomizedListOfGroupToBeRandomized[ramdomizedListIndex]) {
                listOfGroup[i].push(el)
            }
            ramdomizedListIndex++
        }
    }
    // console.log('filled listOfGroup: ', JSON.stringify(listOfGroup, null, 4))

    // 5.listOfGroup을 flat화하여 배열의 원소가 모두 screen 단위가 되도록 한다.
    let resultList = []
    for (let group of listOfGroup) {
        for (let screen of group) {
            resultList.push(screen)
        }
    }
    // console.log('final randomized list: ', JSON.stringify(resultList, null, 4))

    return resultList
}

let shuffle = function (array) {
  let i = 0, j = 0, temp = null
  for (i = array.length - 1; i > 0; i -= 1) {
    j = Math.floor(Math.random() * (i + 1))
    temp = array[i]
    array[i] = array[j]
    array[j] = temp
  }
}

let getStats = function (currentScreenIndex) { // 현재 문제가 속한 part 및 type 에 대한 현재 문제(screenList[screenIndex])까지의 정답률을 구한다.
    let currentScreen = screenList[currentScreenIndex]
    if (!(env.displayStats && env.statsDuration && currentScreen.phase === 'answer' && currentScreen.variables.rightOption)) return undefined
    
    let currentPart = currentScreen.variables.part
    let currentType = currentScreen.variables.type
    let rightOptionDefinedCount = 0 // 현재 part 및 type에 속하는 이제까지의 답변화면들 중 variables.rightOption 이 존재하는 (정답이 정의된) 답변화면의 수
    let rightCount = 0 // 현재 part 및 type에 속하는 이제까지의 답변화면들 중 result.isRight === true인 (정답을 선택한) 화면의 수 
    
    for (let i = 0; i <= currentScreenIndex; i++) {
        let screen = screenList[i]
        if (screen.phase === 'answer' &&
            screen.variables.part === currentPart &&
            screen.variables.type === currentType &&
            screen.variables.rightOption) {
            rightOptionDefinedCount++
            if (screen.result && screen.result.isRight) rightCount++
        }
    }

    if (rightOptionDefinedCount === 0) return undefined // 이번 파트/타입에서는 현재까지 정답이 정의된 답변화면이 하나도 없었음.
    let statsText = currentScreen.result && currentScreen.result.isRight ? `${env.rightAnswerNotice} ` : `${env.wrongAnswerNotice} `
    statsText += "정답률: " + (parseInt(rightCount / rightOptionDefinedCount * 100)) + "%"
    return statsText
}

let setProgressBar = function (percent) {
    document.getElementById("progressbar").style.width = percent + '%'
}

let setRemaining = function (timeLeft) {
    document.getElementById("remaining").innerHTML = timeLeft
}

let contentTarget
let fileName

let init = function () {
    contentTarget = document.getElementById("content")

    let date = new Date()
    fileName = "korean_" + date.getFullYear().toString() + pad2(date.getMonth() + 1) + pad2(date.getDate()) + pad2(date.getHours()) + pad2(date.getMinutes()) + pad2(date.getSeconds())
    if (env.computerName) fileName += "_" + env.computerName
    fileName += ".json"

    if (env.randomOrdered) {
        screenList = randomizeOrder(screenList)
    } else {
        for (let screen of screenList) {
            if (screen.variables && screen.variables.no) screen.variables.seq = screen.variables.no // 순서 랜덤화하지 않으면 따로 seq값을 만들지 않으므로 no와 똑같이 부여해 준다.
        }
    }
}

init()
doScreenAction(0)

// TODO 남은 시간 표시
// TODO answer phase 에서 keyRequiredBeforeDisplayOptions === true이면 문제 표시 후 스페이스바 눌러야 선택지 표시되도록
// TODO 1~7 중 선택하는 option format에 대한 처리
// TODO 마우스로도 가능하도록