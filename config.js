let env = { // 프로그램 전체에 대한 설정
    "computerName": "pc01", // 결과 저장 파일명 마지막에 붙일 텍스트. 어느 컴퓨터에서 만들어진 파일인지 구별이 필요한 경우 설정
    "randomOrdered": true, // 같은 part 및 type 에 속한 문제들을 랜덤하게 노출되도록 할지
    "displayStats": true, // 통계(정답/오답 및 정답율) 표시 여부
    "statsDuration": 1000, // 통계 표시 유지 시간
    "rightAnswerNotice": "정답입니다.", // 정답있는 문제에서 정답 선택한 경우의 정답률 표시부분에 나타낼 문구
    "wrongAnswerNotice": "틀렸습니다." // 정답있는 문제에서 오답 선택한 경우의 정답률 표시부분에 나타낼 문구
}

// 답 선택 옵션의 값과 대응되는 키보드의 키의 매핑 정의
// 키 매핑을 변경해야 하는 경우: http://www.asquare.net/javascript/tests/KeyCode.html 에서 key code를 알아내고 그 값으로 아래를 수정하면 됨.
let keyCodes = {
    "YN": {
        "83": "Y", // 83번 키('S') 를 누르면 Y 값을 입력한 것으로 인식됨
        "75": "N" // 75번 키('K') 를 누르면 N 값을 입력한 것으로 인식됨
    },
    "7": { // key code 49~55번('1'~'7' 키) 가 그대로 1~7 값에 대응됨
        "49": "1",
        "50": "2",
        "51": "3",
        "52": "4",
        "53": "5",
        "54": "6",
        "55": "7"
    }
}

let defaults = { // 각 screen마다 반복해서 같은 값을 입력하지 않도록 하기 위한 phase종류별 기본 설정값.
    "explanation": { // 각종 설명 화면 phase용 기본 설정
        "minimumDuration": 500, // 이 시간 동안은 스페이스바를 눌러도 다음으로 넘어가지 않는다. 제한 없이 처음부터 넘어갈 수 있게 하려면 0으로 설정한다.
        "duration": 1000, // 이 시간에 도달하면 nextAlertText가 표시된다. 표시하지 않으려면 0으로 설정한다.
        "nextAlertText": "스페이스바를 눌러 다음 화면으로 넘어가세요.",
        "save": false  // 화면 표시될 때 이제까지의 결과를 파일로 저장할지의 여부. 주의: 여기서 true로 설정하면 모든 explanation 화면을 표시할 때마다 저장하게 된다. 적당한 screen에서만 true로 설정할 것.
    },
    "cross": { // 십자 표시 화면 phase용 기본 설정
        "duration": 1000 // 이 시간에 도달하면 다음 화면으로 자동으로 넘어간다.
    },
    "question": { // 문제 화면 phase용 기본 설정
        "minimumDuration": 300, // 이 시간 동안은 스페이스바를 눌러도 다음으로 넘어가지 않는다. 제한 없이 처음부터 넘어갈 수 있게 하려면 0으로 설정한다.
        "duration": 1000, // 이 시간에 도달하면 nextAlertText가 표시된다. 표시하지 않으려면 0으로 설정한다.
        "nextAlertText": "문제를 다 읽었으면 스페이스바를 눌러 답 선택 화면으로 넘어가세요.",
        "timeLimit": 5000, // 제한시간. 이 시간에 도달하면 이 문제(이 문제화면 및 대응하는 답변화면 모두)는 시간초과로 틀린 것으로 처리되고, 이 문제에 대한 답변화면이 있다면 건너뛰고, 그 다음 화면으로 이동한다.
        "timeLimitAlertText": "시간 초과입니다. 다음 문제로 넘어갑니다.",
        "timeLimitAlertTextDuration": 1000 // 제한시간 도달시 timeLimitAlertText를 얼마동안 표시한 후 넘어갈지. 표시하지 않고 바로 넘어가려면 0으로 설정한다.
    },
    "answer": { // 답 입력 화면 phase용 기본 설정
        "minimumDuration": 100, // 이 시간 동안은 키보드로 답을 선택해도 무시한다. (오동작 방지용) 제한하지 않으려면 0으로 설정한다.
        "duration": 1000, // 이 시간에 도달하면 nextAlertText가 표시된다. 표시하지 않으려면 0으로 설정한다.
        "nextAlertText": "답을 선택하세요.",
        "timeLimit": 6000, // 제한시간. 이 시간에 도달하면 이 답변은 시간초과로 틀린 것으로 처리되고, 다음 스크린으로 이동한다.
        "timeLimitAlertText": "시간 초과입니다. 다음 문제로 넘어갑니다.",
        "timeLimitAlertTextDuration": 1000, // 제한시간 도달시 timeLimitAlertText를 얼마동안 표시한 후 넘어갈지. 표시하지 않고 바로 넘어가려면 0으로 설정한다.
        "keyRequiredBeforeDisplayOptions": false, // questionText 표시 후 스페이스바를 눌러야 하단의 선택지를 표시할지의 여부. false면 한꺼번에 전부 표시
        "mouseEnabled": true, // 마우스로 답 선택 가능한지
        "save": false
    }
}

let screenList = [
    {
        "phase": "explanation",
        "variables": {
            "guideText":
            `<div align="center">
                <h1>시험과정 소개</h1>
            </div>
            <div class="jumbotron">
                <h4>안녕하세요, 실험 참여자 여러분.
                    여러분은 지금부터 총 세 개의 시험을 보게 될 것입니다.
                    이 시험들은 모두 컴퓨터를 통해 볼 것입니다.
                    이 시험들은 3개의 파트(part)로 나누어져 있습니다.
                    3개의 파트는 다음과 같습니다.</h4>
                <h3>Part I</h3>
                <h4>한국어 읽기 시험 I: 문제 160개</h4>
                <h3>Part II</h4>
                <h4>기억력 게임: 20분</h4>
                <h3>Part III</h3>
                <h4>한국어 읽기 시험 II: 문제 50개</h4>
            </div>`
        }
    },
    {
        "phase": "cross"
    },
    {
        "phase": "question",
        "variables": {
            "part": 1,
            "type": "연습문제",
            "no": 1,
            "questionText": "동료들이 회사에 야근을 하고 있어요."
        }
    },
    {
        "phase": "answer",
        "variables": {
            "part": 1,
            "type": "연습문제",
            "no": 1,
            "questionText": "동료들이 퇴근을 했습니까? 정답은 Y",
            "optionFormat": "YN",
            "rightOption": "Y" // 정답. 정답이 없는 문제 (1~7 고르는 경우 등) 의 경우는 undefined 로 설정한다.
        }
    },
    {
        "phase": "question",
        "variables": {
            "part": 1,
            "type": "연습문제",
            "no": 2,
            "questionText": "농부들은 비는 오기를 간절히 기다리고 있다."
        }
    },
    {
        "phase": "answer",
        "variables": {
            "part": 1,
            "type": "연습문제",
            "no": 2,
            "questionText": "비가 많이 왔습니까?",
            "optionFormat": "7",
            "rightOption": undefined // 정답이 없는 경우의 예. 이때는 결과값에 isRight(정답여부)는 포함되지 않는다.
        }
    },
    {
        "phase": "question",
        "variables": {
            "part": 1,
            "type": "연습문제",
            "no": 3,
            "questionText": "가갸거겨고교"
        }
    },
    {
        "phase": "answer",
        "variables": {
            "part": 1,
            "type": "연습문제",
            "no": 3,
            "questionText": undefined, // 답변화면 상단의 텍스트를 표시하지 않을 경우 undefined로 설정한다.
            "optionFormat": "7",
            "rightOption": undefined // 정답이 없는 경우의 예. 이때는 결과값에 isRight(정답여부)는 포함되지 않는다.
        }
    },
    {
        "phase": "explanation",
        "variables": {
            "guideText":
            `<div class="jumbotron">
                <h3>이제부터 진짜 시험이 시작됩니다.</h3>
                <h3>문제를 푸는 방법은 앞에서 한 연습과 같습니다.</h3>
                <h3>문제는 총 160개입니다.</h3>
                <h3>시작할 준비가 되었으면 스페이스 바를 눌러 주세요.</h3>
            </div>`,
            "minimumDuration": 0
        }
    },
    {
        "phase": "question",
        "variables": {
            "part": 1,
            "type": "문제",
            "no": 1,
            "questionText": "동료들이 회사에 야근을 하고 있어요."
        }
    },
    {
        "phase": "answer",
        "variables": {
            "part": 1,
            "type": "문제",
            "no": 1,
            "questionText": "동료들이 퇴근을 했습니까? 정답은 Y임",
            "optionFormat": "YN",
            "rightOption": "Y"
        }
    },
    {
        "phase": "question",
        "variables": {
            "part": 1,
            "type": "문제",
            "no": 2,
            "questionText": "농부들은 비는 오기를 간절히 기다리고 있다."
        }
    },
    {
        "phase": "answer",
        "variables": {
            "part": 1,
            "type": "문제",
            "no": 2,
            "questionText": "비가 많이 왔습니까? 정답은 N",
            "optionFormat": "YN",
            "rightOption": "N"
        }
    },
    {
        "phase": "question",
        "variables": {
            "part": 1,
            "type": "문제",
            "no": 3,
            "questionText": "가갸거겨고교"
        }
    },
    {
        "phase": "answer",
        "variables": {
            "part": 1,
            "type": "문제",
            "no": 3,
            "questionText": "나냐너녀노뇨?",
            "optionFormat": "YN",
            "rightOption": undefined
        }
    },
    {
        "phase": "cross"
    },
    {
        "phase": "question",
        "variables": {
            "part": 1,
            "type": "문제",
            "no": 4,
            "questionText": "가갸거겨고교"
        }
    },
    {
        "phase": "answer",
        "variables": {
            "part": 1,
            "type": "문제",
            "no": 4,
            "questionText": "나냐너녀노뇨?",
            "optionFormat": "7",
            "rightOption": undefined
        }
    },
    {
        "phase": "answer",
        "variables": {
            "part": 1,
            "type": "문제",
            "no": 5,
            "questionText": "question 스크린 없이 answer스크린만 이용하는 경우이면서, 텍스트 표시 후 스페이스바를 눌러야 선택지가 표시되는 경우의 예입니다.",
            "optionFormat": "7",
            "rightOption": undefined,
            "keyRequiredBeforeDisplayOptions": true, // true: 상단 텍스트 표시 후 스페이스바를 눌러야 하단 선택지를 표시
            "nextAlertText": "문제를 다 읽은 후 스페이스바를 누르면 보기가 표시됩니다.",
        }
    },
    {
        "phase": "explanation",
        "variables": {
            "save": true,
            "guideText":
            `<div class="jumbotron" align="center">
                <h3>한국어 읽기 시험 I이 끝났습니다.</h3>
                <h3>수고하셨습니다!</h3>
            </div>`
        }
    }
]